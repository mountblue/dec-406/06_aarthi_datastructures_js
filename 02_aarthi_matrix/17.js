/*
Given an incomplete Sudoku configuration in terms of a 9x9 2-D square matrix (mat[][]) 
the task to check if the configuration has a solution or not.
*/
var sudoko="3 0 6 5 0 8 4 0 0 5 2 0 0 0 0 0 0 0 0 8 7 0 0 0 0 3 1 0 0 3 0 1 0 0 8 0 9 0 0 8 6 3 0 0 5 0 5 0 0 9 0 6 0 0 1 3 0 0 0 0 2 5 0 0 0 0 0 0 0 0 7 4 0 0 5 2 0 6 3 0 0";
var arraySu=[];
for (var i = 0; i <9; i++) {
	arraySu[i]=new Array(9);
}
var k=0
for (var i = 0; i <9; i++) {
	for (var j = 0; j <9; j++) {
		arraySu[i][j]=sudoko[k];
		k=k+2
	}
}
console.log(sudokuConfig());
function sudokuConfig() {
	for (var i = 0; i <9; i++) {
		var num1=["1","2","3","4","5","6","7","8","9"];
		var num2=["1","2","3","4","5","6","7","8","9"];
		for (var j = 0; j <9; j++) {
			if(arraySu[i][j]!=0){
				var index = num1.indexOf(arraySu[i][j]);
				if(index!=-1){
					num1.splice(index,1);
				}
				else{
					return 0;
				}
			}
			if(arraySu[j][i]!=0){
				var index = num2.indexOf(arraySu[j][i]);
				if(index!=-1){
					num2.splice(index,1);
				}
				else{
					return 0;
				}
			}
		}
	}
	return 1;
}