/*
Given an square matrix, turn it by 90 degrees in 
anti-clockwise direction without using any extra space.
*/
var T=1;
var m=3;
var M=[1, 2, 3, 4, 5, 6, 7, 8, 9];
var array1=[];
for (var i = 0; i <m; i++) {
	array1[i]=new Array(m);
}
var array2=[];
for (var i = 0; i <m; i++) {
	array2[i]=new Array(m);
}
var k=0;
for (var i = 0; i <m; i++) {
	for (var j = 0; j <m; j++) {
		array1[i][j]=M[k];
		k=k+1;
	}
}
console.log(array1[0]);
rotateAntiClockwise();
var res;
function rotateAntiClockwise(){
	for (var j = 0; j < m; j++ ){
		for (var i = 0; i < m; i++){
			array2[i][j]=array1[j][m-i-1];
		}
	}
	res="";
	for (var j = 0; j < m; j++ ){
		for (var i = 0; i < m; i++){
			res+=array2[j][i]+" ";
		}
	}
	console.log(res);
}