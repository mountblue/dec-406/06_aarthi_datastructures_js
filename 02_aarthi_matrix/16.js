/*
There is the Rectangular path for a Train to travel consisting of n and m rows and columns respectively. 
The train will start from one of grid cells and it will be given a command in the form of String s. 
consisting of characters ‘L’, ‘R’, ‘U’, ‘D’.
The train will follow the instructions of the command string, 
where 'L' corresponds moving to the left, 'R' towards the right, 'U' for moving up, and 'D' means down.
You have already selected the command string s, and are wondering if it is possible to place the train 
in one of the grid cells initially and have it always stay entirely within the grid upon 
execution of the command string s. Output “1” if there is a starting cell for which the train doesn’t 
fall off the grid(track) on following command s, otherwise, output "0".
*/
var train=[];
var str="LLRU"
var n=2;
var m=3;
for (var i = 0; i < n; i++ ) {
	train[i]=new Array(m);
}
console.log(TrainProb());
function TrainProb() {
	var x,y;
	for (var i = 0; i < n; i++) {
		for (var j = 0; j <m; j++) {
			for(var k=0; k<str.length; k++){
				if(str[k]=="L"){
					y=j-1;
				}
				if(str[k]=="R"){
					y=j+1;
				}
				if(str[k]=="U"){
					x=i-1;
				}
				if(str[k]=="D"){
					x=i+1;
				}
			}
			if(x<n && x>=0 && y<m && y>=0){
				return 1;
			}
		}
	}
	return 0;
}