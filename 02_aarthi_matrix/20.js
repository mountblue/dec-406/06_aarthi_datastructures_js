/*
Given a 2D matrix of size M*N. Traverse and print the matrix in spiral form.
*/
var T=1;
var m=4;
var n=4;
var M=[1, 2, 3, 4, 5, 6,7, 8, 9, 10, 11, 12,13, 14, 15, 16];
var array1=[];
for (var i = 0; i <m; i++) {
	array1[i]=new Array(m);
}
var k=0;
for (var i = 0; i <m; i++) {
	for (var j = 0; j <n; j++) {
		array1[i][j]=M[k];
		k=k+1;
	}
}
spiralForm();
function spiralForm() {
	var k=0;
	var l=0;
	var res;
	res="";
	while(k<m && l<n){
		for (var i = l ; i < n; i++) {
			res+=(array1[k][i]+" ");
		}
		k++;
		
		for (var i = k; i < m; i++) {
			res+=(array1[i][n-1]+" ");
		}
		n--;
		if(k<m){
			for (var i = n-1; i > l-1; i--) {
				res+=(array1[m-1][i]+" ");
			}
			m--;
		}
		
		if(l<n){
			for (var i = m-1; i > k-1; i--) {
				res+=(array1[i][l]+" ");			}
			l++;
		}
	}
	console.log(res);
}