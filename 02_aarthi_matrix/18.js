/*
Given a N X N matrix (M) filled with 1 , 0 , 2 , 3 . 
Your task is to find whether there is a path possible from source to destination, 
while traversing through blank cells only. You can traverse up, down, right and left.
A value of cell 1 means Source.
A value of cell 2 means Destination.
A value of cell 3 means Blank cell.
A value of cell 0 means Blank Wall.
*/
var array=[];
var flag=false;
var str="0 3 2 3 3 0 1 3 0";
var m=3;
var n=3;
for (var i = 0; i <n; i++) {
	array[i]=new Array(m);
}
var k=0;
var sourcex,sourcey,destx,desty;
for (var i = 0; i <n; i++) {
	for (var j = 0; j <m; j++) {
		array[i][j]=str[k];
		if(str[k]=="1"){
			sourcex=i;
			sourcey=j;
		}
		if(str[k]=="2"){
			destx=i;
			desty=j;
		}
		k=k+2;
	}
}
checkPath(sourcex,sourcey);
function checkPath(x,y) {
	var path=[x,y];
	if(x!=m-1 && array[x+1][y]!="0" && array[x+1][y]!="1") {
		path.push(x+1);
		path.push(y);
	}
	if(x!=0 && array[x-1][y]!="0" && array[x-1][y]!="1") {
		path.push(x-1);
		path.push(y);
	}
	if(y!=n-1 && array[x][y+1]!="0" && array[x][y+1]!="1") {
		path.push(x);
		path.push(y+1);
	}
	if(y!=0 && array[x][y-1]!="0" && array[x][y-1]!="1") {
		path.push(x);
		path.push(y-1);
	}
	array[path[0]][path[1]]=0;
	path.splice(0,2);
	for (var i = 0; i < path.length; i=i+2) {
		var res=array[path[i]][path[i+1]];
		if(res=="2"){
			console.log("Yes");
			flag=true;
		}
	}
	for (var i = 0; i < path.length; i=i+2) {
		if(!flag){
			checkPath(path[i],path[i+1]);
		}
	}
	
}
