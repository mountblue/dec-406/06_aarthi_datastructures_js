/*
This is a functional problem . 
Your task is to return the product of array elements under a given modulo.
The modulo operation finds the remainder after division of one number by another. 
For example, K(mod(m))=K%m= remainder obtained when K is divided by m.
*/
var T=1;
var N=4;
var A=[1, 2, 3, 4];
var prod=1;
for(var i=0; i<N; i++){
	prod=prod*A[i];
}
console.log(prod);