/*
Given an array A of size N, construct a Sum Array S(of same size) such that S is equal to the sum of all the elements of A except A[i]. 
Your task is to complete the function SumArray(A, N) which accepts the array A and N(size of array).
*/
var no=2;
var len1=5;
var arr1=[3, 6, 4, 8, 9];
var len2=6;
var arr2=[4, 5, 7, 3, 10, 1];
SumArray(arr1,len1);
SumArray(arr2,len2);
function SumArray(A,N){
	var sumA=A.reduce((a, b) => a + b, 0);
	for (var i = 0; i < N; i++) {
		A[i]=sumA-A[i];
	}
	console.log(A);
}


