/*
Given two arrays and a number x, find the pair whose sum is 
closest to x and the pair has an element from each array.
*/
var T=2;
var ans;
var N1=4;
var N2=4;
var A1=[1, 4, 5, 7];
var A2=[10, 20, 30, 40];
var X=32;
ans=printClosest(A1,A2,N1,N2,X);
var N1=4;
var N2=4;
var A1=[1, 4, 5, 7];
var A2=[10, 20, 30, 40];
var X=50;
ans=printClosest(A1,A2,N1,N2,X);
function printClosest(A1,A2,N1,N2,X) {
	var diff=Number.MAX_SAFE_INTEGER;
	l = 0;
    r = N2-1;
    while(l < N1 && r >= 0){
        if (Math.abs(A1[l] + A2[r] - X) < diff){
        	res_l = l ;
            res_r = r ;
            diff = Math.abs(A1[l] + A2[r] - X);	
        } 
        if (A1[l] + A2[r] > X){
            r=r-1;
        }
        else{
            l=l+1;
        }
    }
    console.log(A1[res_l]);
    console.log(A2[res_r]);
}