// Node class
	class Node {
		constructor(data)
		{
			this.data = data;
			this.left = null;
			this.right = null;
		}
	}
	
	// Binary Search tree class
	class Tree {
		constructor() {
		// root of a binary seach tree
		this.root = null;
	}
	
    // returns root of the tree
    getRootNode() {
	    return this.root;
    }

}


// Returns sum of the tree 
function findSum(root) { 
    // Base case 
    if (root == null) 
        return 0; 

    if(root !== null) {
		findSum(root.left);
		findSum(root.right);
        return sum += root.data;
	}  
}

//tilt of node of tree
function node_tilt(root) {
    sum = 0;
    var leftSum = findSum(root.left);
    sum = 0;
    var rightSum = findSum(root.right);
    return (Math.abs(leftSum-rightSum));
}


//tilt of tree
function tree_tilt(root) {
    // Base case 
    if (root == null) 
        return 0; 

    if(root !== null) {
		tree_tilt(root.left);
		tree_tilt(root.right);
        array.push(node_tilt(root));
	} 
}

// create an object for the Tree
var tree = new Tree();
tree.root = new Node(1);
tree.root.left = new Node(2);
tree.root.right = new Node(3);
tree.root.left.left = new Node(4);
tree.root.left.right = new Node(5)
tree.root.right.left = new Node(6);
tree.root.right.right = new Node(7);   
var array = [];        
var sum = 0;
var tilt_Sum = 0;
var rootNode = tree.getRootNode();
tree_tilt(rootNode);
for(var i=0; i<array.length;i++)
    tilt_Sum += array[i];
console.log("tilt of every node ="+tilt_Sum);
