// Node class
	class Node {
		constructor(data)
		{
			this.data = data;
			this.left = null;
			this.right = null;
		}
	}
	
	// Binary Search tree class
	class Tree {
		constructor() {
		// root of a binary seach tree
		this.root = null;
	}
	
    // returns root of the tree
    getRootNode() {
	    return this.root;
    }

}

//function to find the Lowest common ancestor of two given values n1 and n2. 
function lowest_common_ansester(root,n1,n2) { 
    // Base case 
    if (root == null) 
        return null; 

    // If either n1 or n2 matches with  root's key.
    if (root.data == n1 || root.data == n2) 
        return root; 

    // Look for keys in left and right subtrees 
    var left = lowest_common_ansester(root.left, n1, n2); 
    var right = lowest_common_ansester(root.right, n1, n2); 

    // If both of the above calls return  Non-NULL, then one key is present  
    // in once subtree and other is present  in other, So this node is the LCA 
    if (left && right) 
        return root; 

    // Otherwise check if left subtree or right  subtree is LCA 
    return (left != NULL) ? left : right; 
}
// create an object for the Tree
var tree = new Tree();
tree.root = new Node(14);
tree.root.left = new Node(2);
tree.root.right = new Node(21);
tree.root.left.left = new Node(1);
tree.root.left.right = new Node(3)
tree.root.right.left = new Node(20);
tree.root.right.right = new Node(45);
