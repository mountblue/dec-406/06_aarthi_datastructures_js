class Node {
	constructor(data)
	{
		this.data = data;
		this.left = null;
		this.right = null;
	}
}
				
// Binary Search tree class
class Tree {
	constructor() {
	// root of a binary seach tree
	this.root = null;
    }
    //inorder traversal
    inorder(node) {
		if(node !== null) {
		this.inorder(node.left);
		console.log(node.data);
		this.inorder(node.right);
		}
	}	
    // returns root of the tree
    getRootNode() {
	    return this.root;
    }

}

//size of tree
function size(node) {   
    if (node==null)  
        return 0; 
    else     
        return(size(node.left) + 1 + size(node.right));   
}

// Returns maximum value in a given Binary Tree 
function findMax(root) { 
    // Base case 
    if (root == null) 
        return -100000; 

    var res = root.data; 
    var lres = findMax(root.left); 
    var rres = findMax(root.right); 
    if (lres > res) 
        res = lres; 
    if (rres > res) 
        res = rres; 
    return res; 
}

// Returns minimum value in a given Binary Tree 
function findMin(root) 
{ 
    // Base case 
    if (root == null) 
        return 100000; 

    var res = root.data; 
    var lres = findMin(root.left); 
    var rres = findMin(root.right); 
    if (lres < res) 
        res = lres; 
    if (rres < res) 
        res = rres; 
    return res; 
}

/* Returns true if a binary tree is a binary search tree */ 
function isBST(node) {  
    if (node == null)  
        return(true);  
    
    /* false if the max of the left is > than us */
    if (node.left!=null && findMax(node.left) > node.data)  
        return(false);  
        
    /* false if the min of the right is <= than us */
    if (node.right!=null && findMin(node.right) < node.data)  
        return(false);  
        
    /* false if, recursively, the left or right is not a BST */
    if (!isBST(node.left) || !isBST(node.right))  
        return(false);  
        
    /* passing all that, it's a BST */
return(true);  
}

// create an object for the Tree
var tree = new Tree();
tree.root = new Node(7);
tree.root.left = new Node(2);
tree.root.right = new Node(5)
tree.root.left.left = new Node(1);
tree.root.left.right = new Node(3)
var rootNode = tree.getRootNode();
console.log(isBST(rootNode));