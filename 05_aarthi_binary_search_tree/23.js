
// Node class
class Node {
	constructor(data)
	{
		this.data = data;
		this.left = null;
		this.right = null;
	}
}

// Binary Search tree class
class BinarySearchTree {
	constructor() {
	// root of a binary seach tree
	this.root = null;
	}


	//insert the node helper method which creates a new node to be inserted and calls insertNode
	insert(data) {
		// Creating a node and initailising with data
		var newNode = new Node(data);
		// root is null then node will be added to the tree and made root.
		if(this.root === null)
			this.root = newNode;
		else
			// find the correct position in the tree and add the node
			this.insertNode(this.root, newNode);
	} 
	
	// Method to insert a node in a tree it moves over the tree to find the location to insert a node with a given data
	insertNode(node, newNode) {
		// if the data is less than the node data move left of the tree
		if(newNode.data < node.data) {
			// if left is null insert node here
			if(node.left === null)
				node.left = newNode;
			else
				// if left is not null recurr until null is found
				this.insertNode(node.left, newNode);
		}
		else {
		// if the data is more than the node data move right of the tree
			// if right is null insert node here
			if(node.right === null)
				node.right = newNode;
			else
				// if right is not null recurr until null is found
				this.insertNode(node.right,newNode);
		}
	}
	
	
	inorder(node) {
		if(node !== null) {
            this.inorder(node.left);
            console.log(node.data);
            this.inorder(node.right);
		}
	}

	
    // returns root of the tree
    getRootNode() {
        return this.root;
    }

    merge(node) {
        if(node !== null) {
            this.merge(node.left);
            this.insert(node.data);
            this.merge(node.right);
        }
    }		
}

// create an first object for the BinarySearchTree
var BST1 = new BinarySearchTree();
// Inserting nodes to the BinarySearchTree
BST1.insert(41);
BST1.insert(20);
BST1.insert(17);
BST1.insert(12);
BST1.insert(24);
BST1.insert(9);
BST1.insert(45);
BST1.insert(52);
BST1.insert(31);
var root1 = BST1.getRootNode();
BST1.inorder(root1);
// create an first object for the BinarySearchTree
var BST2 = new BinarySearchTree();
// Inserting nodes to the BinarySearchTree
BST2.insert(18);
BST2.insert(11);
BST2.insert(43);
BST2.insert(23);
BST2.insert(54);
BST2.insert(56);
BST2.insert(22);
BST2.insert(1);
BST2.insert(6);
var root2 = BST2.getRootNode();
BST2.inorder(root2);
//for merge the tree
var BST = new BinarySearchTree();
var root = BST1.getRootNode();
BST.merge(root);
root = BST2.getRootNode();
BST.merge(root);
root = BST.getRootNode();
BST.inorder(root);
