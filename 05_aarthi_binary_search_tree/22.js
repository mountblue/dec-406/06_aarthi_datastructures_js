// Node class 
class Node 
{ 
    constructor(data) 
    { 
        this.data = data; 
        this.left = null; 
        this.right = null; 
    } 
}

// Binary Search tree class 
class Tree 
{ 
    constructor() 
    { 
        // root of a binary seach tree 
        this.root = null; 
    } 

  	// returns root of the tree 
	getRootNode() 
	{ 
	    return this.root; 
	}

  	// Performs inorder traversal of a tree 
	inorder(node) 
	{ 
	    if(node !== null) 
	    { 
	        this.inorder(node.left); 
	        console.log(node.data); 
	        this.inorder(node.right); 
	    } 
	} 
	
	// Performs preorder traversal of a tree     
	preorder(node) 
	{ 
	    if(node != null) 
	    { 
	        console.log(node.data); 
	        this.preorder(node.left); 
	        this.preorder(node.right); 
	    } 
	} 
	
	// Performs postorder traversal of a tree 
	postorder(node) 
	{ 
	    if(node != null) 
	    { 
	        this.postorder(node.left); 
	        this.postorder(node.right); 
	        console.log(node.data); 
	    } 
	}

	
	size(node) 
	{   
        if (node==null)  
            return 0; 
        else     
            return(tree.size(node.left) + 1 + tree.size(node.right));   
    }

    remove(X)
    {
    	this.root = this.removeNode(this.root, X);
    }
    findMinNode(node) 
    {
		// if left of a node is null then it must be minimum node
		if(node.left === null)
			return node;
		else
			return this.findMinNode(node.left);
	}

    removeNode(node,X)
    {
    	if(node==null){
    		return null;
    	}
    	else if(X<node.data){
    		node.left=tree.removeNode(node.left,X);
    		return node;
    	}
    	else if(X>node.data){
    		node.right=tree.removeNode(node.right,X);
    		return node;
    	}
    	else{
    		if(node.left==null && node.right==null){
    			node=null;
    			return node;
    		}
    		if(node.right==null){
    			node = node.right;
				return node;
			}
			else if(node.right === null) {
				node = node.left;
				return node;
    		}
    		var res = this.findMinNode(node.right);
			node.data = res.data;
			node.right = this.removeNode(node.right, res.data);
			return node;
    	}
    }
}

var tree= new Tree();
tree.root = new Node(4);
tree.root.left = new Node(2);
tree.root.right = new Node(5)
tree.root.left.left = new Node(1);
tree.root.left.right = new Node(3);
var rootnode= tree.getRootNode();
tree.inorder(rootnode);
tree.remove(3);
var rootnode= tree.getRootNode();
tree.inorder(rootnode);
