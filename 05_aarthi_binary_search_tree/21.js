// Node class 
class Node 
{ 
    constructor(data) 
    { 
        this.data = data; 
        this.left = null; 
        this.right = null; 
    } 
}

// Binary Search tree class 
class Tree 
{ 
    constructor() 
    { 
        // root of a binary seach tree 
        this.root = null; 
    } 

  	// returns root of the tree 
	getRootNode() 
	{ 
	    return this.root; 
	}

  	// Performs inorder traversal of a tree 
	inorder(node) 
	{ 
	    if(node !== null) 
	    { 
	        this.inorder(node.left); 
	        console.log(node.data); 
	        this.inorder(node.right); 
	    } 
	} 
	
	// Performs preorder traversal of a tree     
	preorder(node) 
	{ 
	    if(node != null) 
	    { 
	        console.log(node.data); 
	        this.preorder(node.left); 
	        this.preorder(node.right); 
	    } 
	} 
	
	// Performs postorder traversal of a tree 
	postorder(node) 
	{ 
	    if(node != null) 
	    { 
	        this.postorder(node.left); 
	        this.postorder(node.right); 
	        console.log(node.data); 
	    } 
	}

	maxleft(root)
	{
		if (root == null){
                return -100000; 
            }
            var res = root.data; 
            var lres = tree.maxleft(root.left); 
            var rres = tree.maxleft(root.right); 
            if (lres > res) {
                res = lres; 
            }
            if (rres > res) {
                res = rres; 
            }
            return res; 
	}

	minright(root) 
	{ 
        // Base case 
        if (root == null) {
            return 100000; 
        }
        var res = root.data; 
        var lres = tree.minright(root.left); 
        var rres = tree.minright(root.right); 
        if (lres < res) 
            res = lres; 
        if (rres < res) 
            res = rres; 
        return res; 
    } 

	BSTsearch(node)
	{
		if(node==null){
			return true;
		}
		if(node.left!=null && tree.maxleft(node.left)>node.data)
			return false;
		
		if(node.right!=null && tree.minright(node.right<node.data))
			return (false);

        if (!tree.BSTsearch(node.left) || !tree.BSTsearch(node.right))  
            return(false);  
        
        return(true);  
	}

	size(node) 
	{   
        if (node==null)  
            return 0; 
        else     
            return(tree.size(node.left) + 1 + tree.size(node.right));   
    }

	largestBst(node)
	{
		if(node!=null){
			var res=-1;
			var comp;
			if(!(node.left==null && node.right==null)){
			 	tree.largestBst(node.left);
			 	if(tree.BSTsearch(node)){
			 		comp=tree.size(node);
			 		if(comp>res){
			 			res=comp;
			 		}
			 	}
			 	tree.largestBst(node.right);
			}
		}
		return res;
	}  
}
var tree= new Tree();
tree.root = new Node(4);
tree.root.left = new Node(2);
tree.root.right = new Node(5)
tree.root.left.left = new Node(1);
tree.root.left.right = new Node(3);
var rootnode= tree.getRootNode();
//tree.inorder(rootnode);
console.log(tree.largestBst(rootnode));