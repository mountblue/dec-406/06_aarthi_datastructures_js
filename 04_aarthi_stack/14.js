class Stack { 
  
    // Array is used to implement stack 
    constructor() 
    { 
        this.items = []; 
    } 
  
    // Functions to be implemented 
    // push(item) 
    // pop() 
    // peek() 
    // isEmpty() 
    // printStack() 
    // push function 
    push(element) 
    { 
        // push element into the items 
        this.items.push(element); 
    }
    // pop function 
    pop() 
    { 
        // return top most element in the stack 
        // and removes it from the stack 
        // Underflow if stack is empty 
        if (this.items.length == 0) 
            return "Underflow"; 
        return this.items.pop(); 
    } 
    // peek function 
    peek() 
    { 
        // return the top most element from the stack 
        // but does'nt delete it. 
        return this.items[this.items.length - 1]; 
    } 
    // printStack function 
    printStack() 
    { 
        var str = ""; 
        for (var i = 0; i < this.items.length; i++) 
            str += this.items[i] + " "; 
        return str; 
    } 
    // isEmpty function 
    isEmpty() 
    { 
        // return true if stack is empty 
        return this.items.length == 0; 
    }
    nextGreater()
    {
        var temp;
        var sort = new Stack(); 
        for( var i=0; i<this.items.length; i++){
            sort.push(this.items[i]);
        }
        for( var i=0; i<sort.items.length-1; i++){
            for( var j=i+1; j<sort.items.length; j++){
                if(sort.items[i]>sort.items[j]){
                    temp=sort.items[i];
                    sort.items[i]=sort.items[j];
                    sort.items[j]=temp; 
                }
            }
        }  
        console.log(sort.printStack())
        var maxindex;
        var max=sort.items[sort.items.length-1];
        loop2: for (var i = 0; i < this.items.length; i++) {
            loop1: for(var j=0; j<this.items.length; j++){
                if(this.items[i]<sort.items[j]){
                    this.items[i]=sort.items[j];
                    break loop1;
                }
                if(this.items[i]==max){
                    maxindex=i;
                }
            }
        }
        this.items[maxindex]=-1;
        console.log(this.printStack());
    }
}
// creating object for stack class 
var stack = new Stack(); 
stack.push(50);
stack.push(3);
stack.push(35);
stack.push(4);
stack.push(12);
stack.push(6);
stack.push(1);
console.log(stack.printStack());
stack.nextGreater();  

