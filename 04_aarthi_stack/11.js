class Stack { 
  
    // Array is used to implement stack 
    constructor() 
    { 
        this.items = []; 
    } 
  
    // Functions to be implemented 
    // push(item) 
    // pop() 
    // peek() 
    // isEmpty() 
    // printStack() 
    // push function 
    push(element) 
    { 
        // push element into the items 
        this.items.push(element); 
    }
    // pop function 
    pop() 
    { 
        // return top most element in the stack 
        // and removes it from the stack 
        // Underflow if stack is empty 
        if (this.items.length == 0) 
            return "Underflow"; 
        return this.items.pop(); 
    } 
    // peek function 
    peek() 
    { 
        // return the top most element from the stack 
        // but does'nt delete it. 
        return this.items[this.items.length - 1]; 
    } 
    // printStack function 
    printStack() 
    { 
        var str = ""; 
        for (var i = 0; i < this.items.length; i++) 
            str += this.items[i] + " "; 
        return str; 
    } 
    // isEmpty function 
    isEmpty() 
    { 
        // return true if stack is empty 
        return this.items.length == 0; 
    }
    midRemove()
    {
        var len=Math.floor(this.items.length/2);
        for(var i=0; i<this.items.length; i++){
            if(i!=len){
                res.push(this.items[i]);
            }            
        }
        console.log(res.printStack());
    }
}
// creating object for stack class 
var stack = new Stack(); 
stack.push(50);
stack.push(3);
stack.push(35);
stack.push(4);
console.log(stack.printStack());
stack.midRemove();  
var stack = new Stack(); 
stack.push(50);
stack.push(3);
stack.push(35);
stack.push(4);
stack.push(20);
console.log(stack.printStack());
stack.midRemove(); 
