class Stack { 
  
    // Array is used to implement stack 
    constructor() 
    { 
        this.items = []; 
    } 
  
    // Functions to be implemented 
    // push(item) 
    // pop() 
    // peek() 
    // isEmpty() 
    // printStack() 
    // push function 
    push(element) 
    { 
        // push element into the items 
        this.items.push(element); 
    }
    // pop function 
    pop() 
    { 
        // return top most element in the stack 
        // and removes it from the stack 
        // Underflow if stack is empty 
        if (this.items.length == 0) 
            return "Underflow"; 
        return this.items.pop(); 
    } 
    // peek function 
    peek() 
    { 
        // return the top most element from the stack 
        // but does'nt delete it. 
        return this.items[this.items.length - 1]; 
    } 
    // printStack function 
    printStack() 
    { 
        var str = ""; 
        for (var i = 0; i < this.items.length; i++) 
            str += this.items[i] + " "; 
        return str; 
    } 
    // isEmpty function 
    isEmpty() 
    { 
        // return true if stack is empty 
        return this.items.length == 0; 
    }
    Solution()
    {
        var res="";
        var counta=0;
        var countb=0;
        var count=0;
        for (var i = 0; i < this.items.length; i++){
            if(this.items[i]=="("){
                counta++;
                res+=counta;
            }
            if(this.items[i]==")"){
                countb=counta; 
                loop1: while(true){
                    count=0;
                    for(var j=0; j<res.length; j++){
                        if(res[j]==countb+""){
                            count++;
                        }
                    }
                    if(count!=2){
                        res+=countb;
                        break loop1;
                    }
                    else{
                        countb--;
                    }
                }            
                ; 
            }
        }
        console.log(res);
    }
    
}
// creating object for stack class 
var stack = new Stack(); 
var string="(a+(b*c))+(d/e)";
for (var i = 0; i < string.length; i++) {
    stack.push(string[i]);
}
console.log(stack.printStack());
stack.Solution();
var stack = new Stack(); 
var string="((())(()))";
for (var i = 0; i < string.length; i++) {
    stack.push(string[i]);
}
console.log(stack.printStack());
stack.Solution();
